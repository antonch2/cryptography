@echo off

call echo 'Creating virtual environment...'
call python3 -m venv env
call echo 'Created! Activating virtual environment...'
call env/Scripts/activate
call echo 'Activated! Installing requirements...'
call pip install -r requirements.txt
call echo 'Installed! Run program'
call python main.py