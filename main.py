import sys
from PyQt5 import QtWidgets, QtCore
import hashlib
import re
import random
import json
import blind_signature as bs
import ui

PUBK_PATH = "pubk.txt"
PK_PATH = "pk.txt"

def write_in_file(name, data):
    file = open(name.format(), 'w')
    file.write(str(data))
    file.close()


class MyApp(QtWidgets.QMainWindow, ui.UiDialog):

    def __init__(self):
        super().__init__()
        self.setup_ui(self)
        
        # обнуляем все поля при запуске программы
        self.pk_data_from_file = None
        self.pubk_data_from_file = None
        self.data_from_file = None
        self.doc_hash = None
        self.signed_doc = None
        self.signed_blind_doc = None
        self.blind_doc = None
        self.l = None
        self.pubk = None

        # слушатели
        self.generateKeysButton.clicked.connect(self.init_sign_and_voter)
        self.blindButton.clicked.connect(self.mask_doc)
        self.subscribeButton.clicked.connect(self.sing_doc)
        self.unblindButton.clicked.connect(self.unmask_doc)
        self.checkButton.clicked.connect(self.verify_signature)

        self.findPrivateKeyButton.clicked.connect(self.open_pk_file)
        self.findPublicKeyButton.clicked.connect(self.open_pubk_file)
        self.findFileButton2.clicked.connect(self.open_and_save_doc)

        if self.pk_data_from_file is None and self.pk_data_from_file is None:
            self.subscribeButton.setEnabled(False)
            self.findFileButton2.setEnabled(False)
            self.blindButton.setEnabled(False)
            self.unblindButton.setEnabled(False)
            self.checkButton.setEnabled(False)

    def init_sign_and_voter(self):
        try:
            self.signer = bs.Signer()
            self.pubk = self.signer.get_pubk()
            self.voter = bs.Voter(self.pubk['n'], "y")

            self.pk_data_from_file = self.signer.get_pk()
            self.pubk_data_from_file = self.signer.get_pubk()

            write_in_file(PK_PATH, self.pk_data_from_file)
            self.pivateKeyText.setText(PK_PATH)

            write_in_file(PUBK_PATH, self.pubk_data_from_file)
            self.publicKeyText.setText(PUBK_PATH)

            self.statusLabel.setText('созданы ключи')

            self.findFileButton2.setEnabled(True)
        except Exception:
            self.statusLabel.setText('ошибка в создании ключей')

    def mask_doc(self):
        try:
            if self.pubk['n'] is not None and self.pubk['e'] is not None and self.doc_hash is not None:
                self.blind_doc = self.voter.blind_message(self.doc_hash, self.pubk['n'], self.pubk['e'])
                self.statusLabel.setText('Файл замаскирован')
                self.subscribeButton.setEnabled(True)
            else:
                self.statusLabel.setText('Ошибка blind:\nпроверте ключи и файл документа')
        except Exception:
            self.statusLabel.setText('ошибка')

    # Боб подписывает документ
    def sing_doc(self):
        try:
            if self.voter is not None:
                if self.blind_doc is not None:
                    self.signed_blind_doc = self.signer.sign_message(self.blind_doc, self.voter.get_eligibility())
                    self.statusLabel.setText('Замаскированный файл подписан')
                    self.unblindButton.setEnabled(True)
                else:
                    self.statusLabel.setText('Ошибка: замаскируйте файл')
            else:
                self.statusLabel.setText('Ошибка: проверте ключи')
        except Exception:
            self.statusLabel.setText('ошибка')

    # Алиса убирает маскировку
    def unmask_doc(self):
        try:
            if self.pubk['n'] is not None:
                if self.signed_blind_doc is not None:
                    self.signed_doc = self.voter.unwrap_signature(self.signed_blind_doc, self.pubk['n'])
                    self.statusLabel.setText('Файл с подписью получен')
                    self.checkButton.setEnabled(True)
                else:
                    self.statusLabel.setText('Ошибка: подпишите файл')
            else:
                self.statusLabel.setText('Ошибка: проверте ключи')
        except Exception:
            self.statusLabel.setText('ошибка')

    def open_and_save_doc(self):
        try:
            self.file_patch, self.data_from_file = self.get_file(self.fileText2)
            self.aboutSubscribeLabel.setText('')
            self.checkText.setText("")
            if self.l is None:
                self.l = random.randint(1, self.pubk['n'])
            message = self.data_from_file

            concat_message = str(message) + str(self.l)
            message_hash = hashlib.sha256(str(concat_message).encode('utf-8')).hexdigest()
            self.doc_hash = int(message_hash, 16)

            self.statusLabel.setText('Файл загружен')
            self.blindButton.setEnabled(True)
        except Exception:
            self.statusLabel.setText('Ошибка в загрузке файла')

    def open_pubk_file(self):
        try:
            self.pubk_file_patch, key = self.get_file(self.publicKeyText)
            if self.pubk_file_patch is not None:
                json_object = json.loads(str(key).replace("\n\r", "").replace('\'', '\"'))
                public_info = {"n": int(json_object['n']), "e": int(json_object['e'])}
                self.pubk = public_info
                self.voter = bs.Voter(json_object['n'], "y")
                self.statusLabel.setText('Публичный ключ загружен')
                if len(str(self.pivateKeyText)) != 0:
                    self.findFileButton2.setEnabled(True)
        except Exception:
            self.statusLabel.setText('Ошибка в загрузке публичного ключа\nневерный формат')


    def open_pk_file(self):
        try:
            self.pk_file_patch, self.pk_data_from_file = self.get_file_b(self.pivateKeyText)
            if self.pk_file_patch is not None:
                self.signer = bs.Signer(self.pubk, self.pk_data_from_file.decode())
                self.statusLabel.setText('Приватный ключ загружен')
                if len(str(self.publicKeyText)) != 0:
                    self.findFileButton2.setEnabled(True)
        except Exception:
            self.statusLabel.setText('Ошибка в загрузке приватного ключа')


    @staticmethod
    def get_file(textInForm):
        file_patch = QtWidgets.QFileDialog.getOpenFileName()[0]
        if len(file_patch) > 0:
            textInForm.setText(QtCore.QFileInfo(file_patch).fileName())
            fp = open(file_patch)  # rb - Открывает файл для чтения в двоичном формате.
            data_from_file = fp.read()
            return file_patch, data_from_file
        else:
            return None, None

    @staticmethod
    def get_file_b(textInForm):
        file_patch = QtWidgets.QFileDialog.getOpenFileName()[0]
        if len(file_patch) > 0:
            textInForm.setText(QtCore.QFileInfo(file_patch).fileName())
            fp = open(file_patch, 'rb')  # rb - Открывает файл для чтения в двоичном формате.
            data_from_file = fp.read()
            return file_patch, data_from_file
        else:
            return None, None

    def verify_signature(self):
        try:
            if self.pubk is None:
                self.statusLabel.setText('Ошибка: проверте ключи')
            elif self.data_from_file is None:
                self.statusLabel.setText('Ошибка: нет файла')
            elif self.l is None:
                self.statusLabel.setText('Ошибка: файл не был замаскирован')
            elif self.signed_doc is None:
                self.statusLabel.setText('Ошибка:\nфайл не подписан или не размаскирован')
            else:
                verification_status = bs.verify_signature(self.data_from_file, self.l, self.signed_doc,
                                                          self.pubk['e'], self.pubk['n'])
                if verification_status:
                    self.statusLabel.setText('Подпись подтверждена')
                else:
                    self.statusLabel.setText('Подпись не подтверждена')
        except Exception:
            self.statusLabel.setText('ошибка')


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
