import cryptomath
import hashlib
import random

# Генерируем ключи и сохраняем их в файл
def generate_information():
    p = cryptomath.find_prime()
    q = cryptomath.find_prime()
    phi = (p - 1) * (q - 1)
    n = p * q

    found_encryption_key = False
    while not found_encryption_key:
        e = random.randint(2, phi - 1)
        if cryptomath.gcd(e, phi) == 1:
            found_encryption_key = True

    d = cryptomath.find_mod_inverse(e, phi)
    public_info = {"n": n, "e": e}  # Открытый ключ Алисы
    private_info = d

    return [public_info, private_info]

# Алиса
class Signer:

    def __init__(self, public_key=None, private_key=None):
        if public_key is None and private_key is None:
            self.pubk, self.pk = (generate_information())
        else:
            self.pubk, self.pk = (public_key, (int(private_key)))

    def get_pubk(self):
        return self.pubk

    def get_pk(self):
        return self.pk

    # Боб по открытому каналу получает вычисленный m' Алисой
    def sign_message(self, message, eligible):
        # закрытый ключ Боба
        mod_n = self.pubk['n']
        d = self.pk
        if eligible == "y":
            return pow(message, d, mod_n)
        else:
            return None

    def verify_voter(self, eligible):
        pass

# Bob
class Voter:
    def __init__(self, n, eligible):
        self.eligible = eligible
        found_r = False
        while not found_r:
            # случайный маскирющий множитель r
            self.r = random.randint(2, n - 1)
            if cryptomath.gcd(self.r, n) == 1:
                found_r = True

    # маскируем документ
    def blind_message(self, m, n, e):
        blind_m = (m * pow(self.r, e, n)) % n
        return blind_m

    def unwrap_signature(self, signed_blind_message, n):
        r_inv = cryptomath.find_mod_inverse(self.r, n)

        return (signed_blind_message * r_inv) % n

    def get_eligibility(self):
        return self.eligible


def verify_signature(message, rand_n, signature, e, n):
    return int(hashlib.sha256((str(message) + str(rand_n)).encode('utf-8')).hexdigest(), 16) == pow(signature, e, n)
